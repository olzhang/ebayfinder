/**
 * @author Oliver
 */

// Construct the request
// Replace MyAppID with your Production AppID


function makeEbayURL(keyWord, postalCode) {
	var url = "http://www.corsproxy.com/svcs.ebay.com/services/search/FindingService/v1";
	url += "?OPERATION-NAME=findItemsByKeywords";
	url += "&SERVICE-VERSION=1.12.0";
	url += "&SECURITY-APPNAME=OliverIn-39f3-426a-835b-342a95bb1aa9";
	url += "&GLOBAL-ID=EBAY-ENCA";
	url += "&RESPONSE-DATA-FORMAT=JSON";
	url += "&REST-PAYLOAD";
	url += "&keywords="+keyWord;
  	url += "&buyerPostalCode="+postalCode;
   	url += "&paginationInput.entriesPerPage=20";
   	url += "&sortOrder=DistanceNearest";
   	console.log(url);
   	return url;

}


function getPostalCode(keyWord, postalCode) {
	var url = makeEbayURL(keyWord, postalCode);
	var lomk = [];
	$.ajax({
		url: url,
		async: false,
		dataType: 'json',
		success: function(data) {
			var length = data.findItemsByKeywordsResponse[0].searchResult[0].item.length;
			lomk.push(String(data.findItemsByKeywordsResponse[0].searchResult[0].item[0].postalCode));
			lomk.push(String(data.findItemsByKeywordsResponse[0].searchResult[0].item[Math.floor(length/2)].postalCode));
			lomk.push(String(data.findItemsByKeywordsResponse[0].searchResult[0].item[Math.floor(length-1)].postalCode));
		}
	});
	return lomk;
}


function mapURLConstrructor(keyWord, postalCode) {
	var markups = getPostalCode(keyWord, postalCode);
	var url2 = "http://maps.google.com/maps/api/staticmap?";
   	url2 += "size=1024x1024&";
	url2 += "sensor=false&";		
	for(var i=0; i<markups.length; i++) {
		if(i==markups.length-1) {
			url2 += "markers="+markups[i];
		} else {
			url2 += "markers="+markups[i]+"&";
		}	
	}

	/*
	$(document).ready(function() {
		$("<pre/>").text(url2).appendTo('#response');
	})
	*/
	return url2;
}

function getMap(keyWord, postalCode) {
	var urlMap = mapURLConstrructor(keyWord, postalCode);
	$(document).ready(function() {
		$("#map").attr("src", urlMap);
	});
}

/*
$(document).ready(function() {
	$("#button1").click(function() {
		makeEbayURL($("#keyword").val(), $("#adrress").val());
	});
})
*/

$(document).ready(function() {
	$("#button2").on("click", function() {
		getMap($("#keyword").val(), $("#postalcode").val());
	});
})


